using System;
using System.Collections.Generic;
using System.Linq;
using Fishing.Data;
using Fishing.Models;
using Microsoft.EntityFrameworkCore;

namespace Fishing.Repositories
{
    public class ImagesRepository
    {
        private readonly FishingDataContext _context;


        public ImagesRepository(FishingDataContext context)
        {
            _context = context;
        }

        public List<ImageView> GetAll()
        {
            var images = _context.Images
                .Include(x => x.User)
                .Select(i => new ImageView()
                {
                    Id = i.Id,
                    Url = i.Url,
                    UserId = i.UserId,
                }).ToList();

            return images;
        }
        
        public ImageView GetById(int id)
        {
            var images = _context.Images
                .Include(x => x.User)
                .Select(i => new ImageView()
                {
                    Id = i.Id,
                    Url = i.Url,
                    UserId = i.UserId,
                })
                .FirstOrDefault(x => x.Id == id);

            return images;
        }
        
        public ImageView Create(ImageCrudDto imageToCreate)
        {
            var user = _context.Users.Find(imageToCreate.UserId);
            var image = new Image();
            image.Url = imageToCreate.Url;
            image.User = user;
            image.UserId = imageToCreate.UserId;

            _context.Images.Add(image);
            _context.SaveChanges();

            var imageCreated = _context.Images
                .Select(x => new ImageView
                {
                    Id = x.Id,
                    Url = x.Url,
                    UserId = x.UserId
                })
                .FirstOrDefault(i => i.Id == image.Id);
            return imageCreated;
        }
        
        public int Delete(int id)
        {
            var image = _context.Images.FirstOrDefault(x => x.Id == id);

            _context.Remove(image);
            return _context.SaveChanges();
        }
    }
}