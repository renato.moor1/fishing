using System;
using System.Collections.Generic;
using System.Linq;
using Fishing.Data;
using Fishing.Models;
using Microsoft.EntityFrameworkCore;

namespace Fishing.Repositories
{
    public class UsersRepository
    {
        private readonly FishingDataContext _context;


        public UsersRepository(FishingDataContext context)
        {
            _context = context;
        }

        public List<UserViewModel> GetAll()
        {
            var users = _context.Users
                .Include(x => x.UserLikes)
                .Include(x => x.UserLikedBy)
                .Include(u => u.UserLikes)
                .Include(u => u.UserLikedBy)
                .Include(u => u.UserDislikes)
                .Include(u => u.UserDislikedBy)
                .Select(u => new UserViewModel
                {
                    Id = u.Id,
                    Name = u.Name,
                    Bio = u.Bio,
                    UserName = u.UserName,
                    Images = u.Images.Select(i => new ImageView
                    {
                        Id = i.Id,
                        UserId = i.UserId,
                        Url = i.Url
                    }).ToList(),
                    UserLikes = u.UserLikes.Select(x => new UserViewModelMiniature
                    {
                        Id = x.Id,
                        Name = x.Name,
                        Bio = x.Bio,
                        Images = x.Images.Select(i => new ImageView
                        {
                            Id = i.Id,
                            UserId = i.UserId,
                            Url = i.Url
                        }).ToList(),
                    }).ToList(),
                    UserLikedBy = u.UserLikedBy.Select(x => new UserViewModelMiniature
                    {
                        Id = x.Id,
                        Name = x.Name,
                        Bio = x.Bio,
                        Images = x.Images.Select(i => new ImageView
                        {
                            Id = i.Id,
                            UserId = i.UserId,
                            Url = i.Url
                        }).ToList(),
                    }).ToList(),
                    UserDislikes = u.UserDislikes.Select(x => new UserViewModelMiniature
                    {
                        Id = x.Id,
                        Name = x.Name,
                        Bio = x.Bio,
                        Images = x.Images.Select(i => new ImageView
                        {
                            Id = i.Id,
                            UserId = i.UserId,
                            Url = i.Url
                        }).ToList(),
                    }).ToList(),
                    UserDislikedBy = u.UserDislikedBy.Select(x => new UserViewModelMiniature
                    {
                        Id = x.Id,
                        Name = x.Name,
                        Bio = x.Bio,
                        Images = x.Images.Select(i => new ImageView
                        {
                            Id = i.Id,
                            UserId = i.UserId,
                            Url = i.Url
                        }).ToList(),
                    }).ToList(),
                }).ToList();

            return users;
        }

        public User Create(UserCrudDto userToCreate)
        {
            var user = new User();
            user.Name = userToCreate.Name;
            user.Bio = userToCreate.Bio;

            _context.Users.Add(user);
            _context.SaveChanges();

            return user;
        }

        public UserViewModel Login(Login loginUser)
        {
            var userMatchLogin = _context.Users.FirstOrDefault(u => u.UserName == loginUser.UserName && u.Password == loginUser.Password);

            if (userMatchLogin != null)
            {
                var user = _context.Users
                .Include(u => u.UserLikes)
                .Include(u => u.UserLikedBy)
                .Include(u => u.UserDislikes)
                .Include(u => u.UserDislikedBy)
                .Include(u => u.Images)
                .Select(u => new UserViewModel
                {
                    Id = u.Id,
                    Name = u.Name,
                    Bio = u.Bio,
                    UserName = u.UserName,
                    Images = u.Images.Select(i => new ImageView {
                     Id = i.Id,
                     UserId = i.UserId,
                     Url = i.Url
                    }).ToList(),
                    UserLikes = u.UserLikes.Select(x => new UserViewModelMiniature
                    {
                        Id = x.Id,
                        Name = x.Name,
                        Bio = x.Bio,
                        Images = x.Images.Select (i => new ImageView {
                         Id = i.Id,
                         UserId = i.UserId,
                         Url = i.Url
                        }).ToList(),
                    }).ToList(),
                    UserLikedBy = u.UserLikedBy.Select(x => new UserViewModelMiniature
                    {
                        Id = x.Id,
                        Name = x.Name,
                        Bio = x.Bio,
                        Images = x.Images.Select (i => new ImageView {
                         Id = i.Id,
                         UserId = i.UserId,
                         Url = i.Url
                        }).ToList(),
                    }).ToList(),
                    UserDislikes = u.UserDislikes.Select(x => new UserViewModelMiniature
                    {
                        Id = x.Id,
                        Name = x.Name,
                        Bio = x.Bio,
                        Images = x.Images.Select (i => new ImageView {
                         Id = i.Id,
                         UserId = i.UserId,
                         Url = i.Url
                        }).ToList(),
                    }).ToList(),
                    UserDislikedBy = u.UserDislikedBy.Select(x => new UserViewModelMiniature
                    {
                        Id = x.Id,
                        Name = x.Name,
                        Bio = x.Bio,
                        Images = x.Images.Select (i => new ImageView {
                         Id = i.Id,
                         UserId = i.UserId,
                         Url = i.Url
                        }).ToList(),
                    }).ToList(),
                }).ToList()
                .FirstOrDefault(u => u.Id  == userMatchLogin.Id);


            var matches =  user.
                    UserLikes.Where(userLike => 
                        user.UserLikedBy.Any(userLiked => userLiked.Id == userLike.Id)).ToList();

            user.Matches = matches;
            return user;
            }
            
            return new UserViewModel();
        }
        
        public UserViewModel GetById(int id)
        {
            var user = _context.Users
                .Include(u => u.UserLikes)
                .ThenInclude(i => i.Images)
                .Include(u => u.UserLikedBy)
                .Include(u => u.UserDislikes)
                .Include(u => u.UserDislikedBy)
                .Include(u => u.Images)
                .Select(u => new UserViewModel
                {
                    Id = u.Id,
                    Name = u.Name,
                    Bio = u.Bio,
                    UserName = u.UserName,
                    Images = u.Images.Select(i => new ImageView {
                     Id = i.Id,
                     UserId = i.UserId,
                     Url = i.Url
                    }).ToList(),
                    UserLikes = u.UserLikes.Select(x => new UserViewModelMiniature
                    {
                        Id = x.Id,
                        Name = x.Name,
                        Bio = x.Bio,
                        Images = x.Images.Select (i => new ImageView {
                         Id = i.Id,
                         UserId = i.UserId,
                         Url = i.Url
                        }).ToList(),
                    }).ToList(),
                    UserLikedBy = u.UserLikedBy.Select(x => new UserViewModelMiniature
                    {
                        Id = x.Id,
                        Name = x.Name,
                        Bio = x.Bio,
                        Images = x.Images.Select (i => new ImageView {
                         Id = i.Id,
                         UserId = i.UserId,
                         Url = i.Url
                        }).ToList(),
                    }).ToList(),
                    UserDislikes = u.UserDislikes.Select(x => new UserViewModelMiniature
                    {
                        Id = x.Id,
                        Name = x.Name,
                        Bio = x.Bio,
                        Images = x.Images.Select (i => new ImageView {
                         Id = i.Id,
                         UserId = i.UserId,
                         Url = i.Url
                        }).ToList(),
                    }).ToList(),
                    UserDislikedBy = u.UserDislikedBy.Select(x => new UserViewModelMiniature
                    {
                        Id = x.Id,
                        Name = x.Name,
                        Bio = x.Bio,
                        Images = x.Images.Select (i => new ImageView {
                         Id = i.Id,
                         UserId = i.UserId,
                         Url = i.Url
                        }).ToList(),
                    }).ToList(),
                }).ToList()
                .FirstOrDefault(u => u.Id == id);

            var matches =  user.
                    UserLikes
                    .Where(userLike => 
                        user.UserLikedBy.Any(userLiked => userLiked.Id == userLike.Id)).ToList();

            user.Matches = matches;
            return user;
        }


        public int Like(UserAction userAction)
        {
            var currentUser = _context.Users
                .Include(u => u.UserLikes)
                .FirstOrDefault(u => u.Id == userAction.CurrentUser);

            var userLiked = _context.Users
                .Include(u => u.UserLikedBy)
                .FirstOrDefault(u => u.Id == userAction.AffectedUser);

            currentUser.UserLikes.Add(userLiked);
            userLiked.UserLikedBy.Add(currentUser);

            return _context.SaveChanges();
        }

        public int Dislike(UserAction userAction)
        {
            var currentUser = _context.Users
                .Include(u => u.UserLikes)
                .FirstOrDefault(u => u.Id == userAction.CurrentUser);

            var userDisliked = _context.Users
                .Include(u => u.UserLikedBy)
                .FirstOrDefault(u => u.Id == userAction.AffectedUser);

            currentUser.UserDislikes.Add(userDisliked);
            userDisliked.UserDislikedBy.Add(currentUser);

            return _context.SaveChanges();
        }

        public UserViewModelMiniature Next(int userId)
        {
            var currentUser = _context.Users
                .Include(u => u.Images)
                .Include(u => u.UserLikes)
                .Include(u => u.UserDislikes)
                .First(u => u.Id == userId);

            var userLikesIds = currentUser.UserLikes.Select(u => u.Id).ToList();
            
            userLikesIds.AddRange(currentUser.UserDislikes.Select(u => u.Id).ToList());

            var user = _context.Users
                .Include(u => u.Images)
                .Select(u => new UserViewModelMiniature
            {
                Id = u.Id,
                Name = u.Name,
                Bio = u.Bio,
                UserName = u.UserName,
                Images = u.Images.Select (i => new ImageView {
                    Id = i.Id,
                    UserId = i.UserId,
                    Url = i.Url
                }).ToList(),
            }).FirstOrDefault(u => !userLikesIds.Contains(u.Id) && u.Id != userId);

            return user;
        }

        public int UpdateName(UserUpdateName userData)
        {
            var user = _context.Users.FirstOrDefault(u => u.Id == userData.CurrentUser);
            user.Name = userData.Name;
            return _context.SaveChanges();
        }

        public int UpdateBio(UserUpdateBio userData)
        {
            var user = _context.Users.FirstOrDefault(u => u.Id == userData.CurrentUser);
            user.Bio = userData.Bio;
            return _context.SaveChanges();
        }
        
        public int Delete(int id)
        {
            var user = _context.Users.FirstOrDefault(x => x.Id == id);

            _context.Remove(user);
            return _context.SaveChanges();
        }
          
        public bool ExistById(int id)
        {
            return _context.Users.Any(u => u.Id == id);
        }
          
        public bool UserName(string username)
        {
            return _context.Users.Any(u => u.UserName == username);
        }
    }
}