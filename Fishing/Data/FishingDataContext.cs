using Fishing.Models;
using Microsoft.EntityFrameworkCore;

namespace Fishing.Data
{
    public class FishingDataContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Image> Images { get; set; }
        public FishingDataContext(DbContextOptions<FishingDataContext> options)
            : base(options) 
        {
        
        }
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>(entity =>
            {
                entity.HasMany(x => x.UserLikes)
                    .WithMany(x => x.UserLikedBy);
                entity.HasMany(x => x.UserDislikes)
                    .WithMany(x => x.UserDislikedBy);
            });
            
            modelBuilder.Entity<Image>()
                .HasOne(x => x.User)
                .WithMany(x => x.Images);
            
            modelBuilder.Entity<User>()
                .Property(e => e.Id)
                .ValueGeneratedOnAdd();
            
            modelBuilder.Entity<Image>()
                .Property(e => e.Id)
                .ValueGeneratedOnAdd();
        }
    }
}