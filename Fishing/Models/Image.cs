using System.ComponentModel.DataAnnotations;

namespace Fishing.Models
{
    public class Image
    {
        public int Id { get; set; }
        [Required]
        public string Url { get; set; }
        [Required]
        public int UserId { get; set; }
        public User User {get; set;}
    }

    public class ImageView
    {
        public int Id { get; set; }
        public string Url { get; set; }
        public int UserId { get; set; }
    } 
    
    public class ImageCrudDto
    {
        public int UserId { get; set; }
        public string Url { get; set; }
    } 
}