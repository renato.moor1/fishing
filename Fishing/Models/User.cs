using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Fishing.Models
{
    public class User
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Bio { get; set; }
        
        public ICollection<Image> Images { get; set; }
        public ICollection<User> UserLikes { get; set; }
        public ICollection<User> UserLikedBy { get; set; }
        public ICollection<User> UserDislikes { get; set; }
        public ICollection<User> UserDislikedBy { get; set; }
        
        public User()
        {
            this.UserLikes = new List<User>();
            this.UserLikedBy = new List<User>();
            this.UserDislikes = new List<User>();
            this.UserDislikedBy = new List<User>();
        }
    }

    public class UserCrudDto
    {
        [Required] public string Name { get; set; }
        public string Bio { get; set; }
    }
    
    public class UserViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string UserName { get; set; }
        public string Bio { get; set; }

        public ICollection<UserViewModelMiniature> UserLikes { get; set; }
        public ICollection<UserViewModelMiniature> UserLikedBy { get; set; }
        public ICollection<UserViewModelMiniature> UserDislikes { get; set; }
        public ICollection<UserViewModelMiniature> UserDislikedBy { get; set; }

        public ICollection<ImageView> Images { get; set; }
        public ICollection<UserViewModelMiniature> Matches { get; set; }

    }
    
    public class UserViewModelMiniature
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string Name { get; set; }
        public string Bio { get; set; }
        public ICollection<ImageView> Images { get; set; }
    }
    
    public class Login
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }

    public class UserUpdateName
    {
        public int CurrentUser { get; set; }
        public string Name { get; set; }
    }
    
    public class UserUpdateBio
    {
        public int CurrentUser { get; set; }
        public string Bio { get; set; }
    }

    public class UserAction
    {
        public int CurrentUser { get; set; }
        public int AffectedUser { get; set; }
    }
}