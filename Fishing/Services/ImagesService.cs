using System;
using System.Collections.Generic;
using Fishing.Data;
using Fishing.Models;
using Fishing.Repositories;

namespace Fishing.Services
{
    public class ImagesService
    {
        private readonly ImagesRepository _imagesRepository;
        private readonly UsersRepository _usersRepository;

        public ImagesService(ImagesRepository imagesRepository, UsersRepository usersRepository)
        {
            _imagesRepository = imagesRepository;
            _usersRepository = usersRepository;
        }

        public List<ImageView> GetAll()
        {
            return _imagesRepository.GetAll();
        }

        public ImageView GetById(int id)
        {
            return _imagesRepository.GetById(id);
        }
        public int Delete(int id)
        {
            if(id == 0 || id < 0)
                throw new Exception("Invalid id");
            return _imagesRepository.Delete(id);
        }

        public ImageView Create(ImageCrudDto imageToCreate)
        {
            if(String.IsNullOrEmpty(imageToCreate.Url))
                throw new Exception("The name is mandatory");
            if(imageToCreate.UserId < 1)
                throw new Exception("Invalid id");
            if(!_usersRepository.ExistById(imageToCreate.UserId))
                throw new Exception("User doesn't exist");

            return _imagesRepository.Create(imageToCreate);
        }
    }
}