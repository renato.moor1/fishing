using System;
using System.Collections.Generic;
using System.IO;
using Fishing.Data;
using Fishing.Models;
using Fishing.Repositories;

namespace Fishing.Services
{
    public class UsersService
    {
        private readonly UsersRepository _usersRepository;

        public UsersService(UsersRepository usersRepository)
        {
            _usersRepository = usersRepository;
        }

        public List<UserViewModel> GetAll()
        {
            return _usersRepository.GetAll();
        }

        public UserViewModel GetById(int id)
        {
            return _usersRepository.GetById(id);
        }

        public int Like(UserAction userAction)
        {
            return _usersRepository.Like(userAction);
        }
        
        public int Dislike(UserAction userAction)
        {
            return _usersRepository.Dislike(userAction);
        }
        
        public User Create(UserCrudDto userToCreate)
        {
            return _usersRepository.Create(userToCreate);
        }
        
        public UserViewModelMiniature Next(int currentUser)
        {
            return _usersRepository.Next(currentUser);
        }
        
        public int UpdateName(UserUpdateName userData)
        {
            if(String.IsNullOrEmpty(userData.Name))
                throw new Exception("The name is mandatory");
                
            if (!_usersRepository.ExistById(userData.CurrentUser))
                throw new InvalidDataException("No user with this id exist");
            return _usersRepository.UpdateName(userData);
        }
        
        public int UpdateBio(UserUpdateBio userData)
        {
            if (!_usersRepository.ExistById(userData.CurrentUser))
                throw new Exception("No user with this id exist");
            return _usersRepository.UpdateBio(userData);
        }
        
        public UserViewModel Login(Login loginUser)
        {
            return _usersRepository.Login(loginUser);
        }
        
        public int Delete(int id)
        {
            return _usersRepository.Delete(id);
        }

    }
}