using System;
using System.Collections.Generic;
using Fishing.Models;
using Fishing.Services;
using Microsoft.AspNetCore.Mvc;

namespace Fishing.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ImagesController : ControllerBase
    {
        private readonly ImagesService _imagesService;

        public ImagesController(ImagesService imagesService)
        {
            _imagesService = imagesService;
        }
        
        [HttpGet]
        public IActionResult GetAll()
        {
            return Ok(_imagesService.GetAll());
        }
        
        [HttpGet("{id}")]
        public IActionResult GetSingle(int id)
        {
            if(id == 0 || id < 0)
                return BadRequest("Invalid id");
            var image = _imagesService.GetById(id);
            if (image == null)
                return NotFound("Image not found");
            return Ok(image);
        }

        [HttpPost("create")]
        public IActionResult Create(ImageCrudDto imageToCreate)
        {
            try
            {
                var image = _imagesService.Create(imageToCreate);
                return Ok(image);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
        
        [HttpDelete("{id}")]
        public IActionResult Delete([FromRoute] int id)
        {
            try
            {
                var process = _imagesService.Delete(id);
                if (process == 0)
                    return BadRequest("Something went wrong");
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            return NoContent();
        }
    }
}   