using System;
using System.Collections.Generic;
using Fishing.Models;
using Fishing.Services;
using Microsoft.AspNetCore.Mvc;

namespace Fishing.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UsersController : ControllerBase
    {
        private readonly UsersService _usersService;

        public UsersController(UsersService usersService)
        {
            _usersService = usersService;
        }
        
        [HttpGet]
        public IActionResult GetAll()
        {
            return Ok(_usersService.GetAll());
        }
        
        [HttpGet("{id}")]
        public IActionResult GetSingle(int id)
        {
            if(id == 0 || id < 0)
                return BadRequest("Invalid id");
            
            var user =_usersService.GetById(id);
            if (user == null)
                return NotFound("User not found");
            return Ok(user);
        }
               
        [HttpPost("like")]
        public IActionResult Like(UserAction userAction)
        {
            var process = _usersService.Like(userAction);
            if (process < 1)
                return BadRequest("bad");
            return Ok("ok");
        }
        
        [HttpPost("dislike")]
        public IActionResult Dislike([FromBody] UserAction userAction)
        {
            var process = _usersService.Dislike(userAction);
            if (process < 1)
                return BadRequest("bad");
            return Ok("ok");
        }

        [HttpPost("create")]
        public IActionResult Create(UserCrudDto userToCreate)
        {
            var user = _usersService.Create(userToCreate);
            return Ok(user);
        }
        
        [HttpGet("next")]
        public IActionResult GetNextUser(int currentUserId = 0)
        {
            if (currentUserId == 0 && currentUserId < 0)
                return BadRequest("Invalid id");
                    
            var user = _usersService.Next(currentUserId);
            if (user == null)
                return NotFound("No users lefts to displays");
            return Ok(user);
        }
        
        [HttpPost("login")]
        public IActionResult Login([FromBody] Login loginUser)
        {
            var user =  _usersService.Login(loginUser);
            if (user.Id == 0)
                return BadRequest("Incorrect credentials");
            return Ok(user);
        }   
        
        [HttpPost("update/name")]
        public IActionResult UpdateName([FromBody] UserUpdateName userData)
        {
            var process = 0;
            try
            {
                process = _usersService.UpdateName(userData);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            if (process < 1)
                return BadRequest("Cannot update");
            return Ok("ok");
        }

        [HttpPost("update/bio")]
        public IActionResult UpdateName([FromBody] UserUpdateBio userData)
        {
            var process = 0;
            try
            { 
                process = _usersService.UpdateBio(userData);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            if (process < 1)
                return BadRequest("Something wrong happened");
            return Ok("ok");
        }
        
        [HttpDelete("{id}")]
        public IActionResult Delete([FromRoute] int id)
        {
            if(id == 0 || id < 0)
                return BadRequest("Invalid id");
            var process = _usersService.Delete(id);
            if (process == 0)
                return BadRequest("Something went wrong");
            return NoContent();
        }
    }
}   